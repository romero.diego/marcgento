<?php

namespace Marcgento\ModuloBasico\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Marcgento\ModuloBasico\Block\Marcgento;

class Index extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * execute
     *
     * @return this
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed("Marcgento_ModuloBasico::index");
    }

}
